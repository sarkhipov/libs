<?php

/**
 * Класс для снятия метрик приложения. Собирает в процессе работы информацию о расходе памяти и времени работы скрипта.
 * 
 * <b>Основные методы для работы с классом:</b>
 * <ul>
 * 	<li>{@link run()}</li>
 * 	<li>{@link stop()}</li>
 * </ul>
 * 
 * <br/>
 * <p><b>Примеры работы с классом:</b></p>
 * <p>
 * <pre>
 * // запускаем сборщик в начале работы скрипта
 * ACPuls::get()->run();
 * 
 * //  
 * *some code*
 * 
 * // останавливаем сборщик для фиксации и сохранения метрик
 * ACPuls::get()->stop();
 * </pre>
 * </p>
 * @author sarkhipov
 * @package apps3libs
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * 
 */
class ACPuls extends ACLibrary
{

    /** Имя проекта, с которым на данный момент работает библиотека */
    private $project;
    
    /** Алиас сообщения */
    private $alias;
    
    /** Экземпляр класса */        
    private static $_instance; 
    
    /** */
    private $startTime;
    
    /** */
    private $host = '192.168.56.102';
    private $port = '43278';


    /**
     * Приватный конструктор для реализации singleton
     */
    private function __construct() {
        if(defined('APPS3_PROJECT_NAME'))
            $this->setProject(APPS3_PROJECT_NAME);    
    }
    
    /** Приватный clone для запрета клонирования объекта */
    private function __clone(){}
    
    
    /**
     * Возвращает объект класса по имени проекта.
     * Если объект не существует, то создает его.
     * 
     * @param string Имя проекта (при повторных вызовах метода для одного и того же поекта это параметр указывать не обязательно)
     * @return ACCache
     */
    public static function get($alias = null, $project = null) {
        if(self::$_instance===null) {
            self::$_instance = new self();
        }
        
        if($project)
            self::$_instance->setProject($project);
        
        if($alias)
            self::$_instance->setAlias($alias);

        return self::$_instance;
    }
    
    /**
     * Запуск сбора метрик. Фиксирует начальное время работы скрипта.
     * @return bool
     */
    public function run() {
        if(!$this->project)
            return false;
        
        $this->startTime = microtime(true);
        return true;
    }
    
    /**
     * Окончание сбора метрик. Фиксирует конечное время работы скрипта и состояние памяти.
     * Сохраняет собранные метрики.
     * @return bool
     */
    public function stop() {
        if(!$this->project || !$this->startTime)
            return false;
        
        return $this->save();
    }
    
    /**
     * Подготавливает метрики для сохранения.
     * @return array
     */
    private function prepareMetrics() {
        $time = (float)(microtime(true) - $this->startTime);
        if($time < 0) $time = 0;
        return array(
            'm' => memory_get_usage(),
            'pm' => memory_get_peak_usage(),
            'mt' => $time,
            't' => time(),
            'a' => $this->getAlias(),
            'p' => $this->project
        );
    }
    
    /**
     * Сохраняет метрики
     * @return bool
     */
    private function save() {
        try {
            $metrics = $this->prepareMetrics();
            $this->sendSocketMessage(json_encode($metrics));
            return true;
        }
        catch(Exception $e) {
            $this->_addLibLog($e->getMessage(), 'error', __METHOD__);
            return false;
        }
    }

    /**
     * Отправляет сообщение серверу
     * @param string Сообщение
     * @return mixed Результат отправки сообщения
     */
    private function sendSocketMessage($message) {
        try {
            $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
            $result = socket_connect($socket, $this->host, $this->port);
            return socket_send($socket, $message, strlen($message), (int) MSG_EOF);
        }
        catch(Exception $e) {
            return false;
        }
    }

    /**
     * Генерирует ключ по названию проекта для записи данных в Redis.
     * @return string
     */
    private function genKey() {
        return $this->project."_".md5(time().rand(0, 100000));
    }

    /**
     * Устанавливает текущее имя проекта.
     * 
     * @param string Имя проекта
     * @return ACCache
     */
    public function setProject($project) {
        $this->project = $project;
        return $this;
    }
    
    /**
     * Возвращает установленный алиас. Если алиас не установлен, возвращает алиас по умолчанию - url. 
     * 
     * @return string
     */
    public function getAlias() {
        if($this->alias)
            return $this->alias;
        
        $alias = '';
        if(isset($_SERVER['REQUEST_URI'])) {
            $alias = $_SERVER['REQUEST_URI'];
        }
        return $alias;
    }


    /**
     * Устанавливает алиас для набора данных.
     * По умолчанию в качестве алиаса используется url.
     * 
     * @param string Имя проекта
     * @return ACCache
     */
    public function setAlias($alias) {
        $this->alias = $alias;
        return $this;
    }

    /**
     * Внутренний лог работы класса.
     * 
     * @param string Текст сообщения
     * @param string Тип сообщения
     * @param string Алиас 
     */
    private function _addLibLog($message, $level = 'error', $who = '') {
        if(class_exists('ACLogger')) 
            ACLogger::get($this->project)->log($message, $level, 'puls: '.$who);
    }

}


?>
