<?php

/**
 * Интерфейс для классов кеширования.
 * 
 * @author sarkhipov
 * @package apps3libs
 * @subpackage cache
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * 
 */
interface ACCache_InterfaceCachable 
{
        
    /**
     * Запись данных в кеш
     */
    public function write($alias, $data);
    
    /**
     * Чтение данных из кеша
     */
    public function read($alias);

}

?>
