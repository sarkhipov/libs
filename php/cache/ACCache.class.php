<?php

/**
 * Класс для работы с кэшем проектов. Работа с кешированием в файлах и в оперативной памяти.
 * 
 * По умолчанию использует глобальное имя проекта, установленного через Apps3Load::setProject().
 * 
 * <b>Основные методы для кеширования данных:</b>
 * <ul>
 * 	<li>{@link write()}</li>
 * 	<li>{@link read()}</li>
 * </ul>
 * 
 * <br/>
 * <p><b>Примеры работы с классом:</b></p>
 * <p>
 * <pre>
 * // устанавливаем период кешрование в один час
 * ACCache::get()->setCachePeriod(60*60);
 * 
 * // используем кеширование в памяти
 * // по умолчанию используются файлы и метод {@link useFiles()}
 * ACCache::get()->useMemory();
 * 
 * // пробуем забрать нужные данные из кеша
 * $data = ACCache::get()->read('my_data');
 * 
 * if(!$data) {
 *     // если кеш пустой, то получаем данные нужным образом и пишем их в кеш
 *     $data = 'some data from DB';
 *     ACCache::get()->write('my_data', $data);
 * }
 * </pre>
 * </p>
 * @author sarkhipov
 * @package apps3libs
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * 
 */
class ACCache extends ACLibrary
{

    /** 
     * Имя проекта, с которым на данный момент работает кэш. 
     * Если свойство не задано, то кэширование выполняться не будет. 
     */
    private $project;
    
    /** Время хранения кеша в секундах */
    public $cachePeriod;
    
    /** Тип кеширования */
    private $cacheType;
    
    /** Экземпляр логгера */        
    private static $_instance; 
    
    /**
     * Приватный конструктор для реализации singleton
     */
    private function __construct() {
        if(defined('APPS3_PROJECT_NAME'))
            $this->setProject(APPS3_PROJECT_NAME);
        
        $this->setCachePeriod(60);
        $this->useFiles();
    }
    
    /** Приватный clone для запрета клонирования объекта */
    private function __clone(){}
    
    
    /**
     * Возвращает объект класса по имени проекта.
     * Если объект не существует, то создает его.
     * 
     * @param string Имя проекта (при повторных вызовах метода для одного и того же поекта это параметр указывать не обязательно)
     * @return ACCache
     */
    public static function get($project = null) {
        if(self::$_instance===null) {
            self::$_instance = new self();
        }
        
        if($project)
            self::$_instance->setProject($project);

        return self::$_instance;
    }
    

    /**
     * Устанавливает текущее имя проекта.
     * 
     * @param string Имя проекта
     * @return ACCache
     */
    public function setProject($project) {
        $this->project = $project;
        return $this;
    }
    
    
    /**
     * Устанавливает время валидности кеша
     * 
     * @param int Время в секундах
     * @return ACCache
     */
    public function setCachePeriod($seconds) {
        $this->cachePeriod = $seconds;
        return $this;
    }
    
    /**
     * Возвращает полный алиас кеша, с учетом текущего проекта.
     * @param string Алиас кеша
     * @return string 
     */
    public function getFullAlias($alias) {
        return $this->project."_".$alias;
    }
    
    /**
     * Устанавливает тип кеширования
     * 
     * @param string Тип кеширования
     * @return ACCache
     */
    private function setCacheType($type) {
        $this->cacheType = $type;
        return $this;
    }
    
    /**
     * Использование кеширования в файлах 
     * 
     * @param string Тип кеширования
     * @return ACCache
     */
    public function useFiles() {
        return $this->setCacheType('File');
    }
    
    /**
     * Использование кеширования в памяти 
     * 
     * @param string Тип кеширования
     * @return ACCache
     */
    public function useMemory() {
        return $this->setCacheType('Memory');
    }
    
    /**
     * Возвращает объект кеша того типа, который используется на данный момент.
     * @return mixed
     */
    public function getCacher() {
        $cacherType = 'ACCache_'.$this->cacheType;
        if(class_exists($cacherType))
            return $cacherType::get();  
        return false;
    }

    
    /**
     * Запись данных в кеш
     * @param string Алиас кеша
     * @param mixed Кешируемые данные
     * @return bool TRUE, иначе FALSE в случае ошибки 
     * 
     */
    public function write($alias, $data) {
        
        if(!$this->project)
            return false;
        
        $cacher = $this->getCacher();
        if(!$cacher) return false;
        
        try {
            $cacher->write($this->getFullAlias($alias), $this->serialize($data));
            return true;
        }
        catch(Exception $e) {
            $this->_addLibLog($e->getMessage(), 'error', __METHOD__);
            return false;
        }
    }
    
    /**
     * Чтение данных из кеша
     * @param string Алиас кеша
     * @return mixed Исходные данные кеша, либо FALSE в случае ошибки
     */
    public function read($alias) {
        if(!$this->project)
            return false;
        
        $cacher = $this->getCacher();
        if(!$cacher) return false;
        
        $data = $cacher->read($this->getFullAlias($alias));
        if($data)
            return $this->unserialize($data);
        return false;
    }

    /**
     * Сериализация кешируемых данных
     * @param mixed Входные данные
     */
    private function serialize($data) {
        try {
            return json_encode($data);
        }
        catch(Exception $e) {
            return '';
        }
    }

    /**
     * Десериализация кешируемых данных
     * @param mixed Сериализованные данные
     */
    private function unserialize($data) {
        try {
            return json_decode($data);
        }
        catch(Exception $e) {
            return '';
        }
    }   
    /**
     * Внутренний лог работы класса.
     * 
     * @param string Текст сообщения
     * @param string Тип сообщения
     * @param string Алиас 
     */
    public function _addLibLog($message, $level = 'error', $who = '') {
        if(class_exists('ACLogger')) 
            ACLogger::get($this->project)->log($message, $level, 'cache: '.$who);
    }

}


?>
