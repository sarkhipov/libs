<?php

/**
 * Класс для кеширования данных через память. Реализует общий интерфейс кеширования.
 * 
 * @author sarkhipov
 * @package apps3libs
 * @subpackage cache
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * 
 */
class ACCache_Memory implements ACCache_InterfaceCachable {
    
    private static $_instance;

    /**
     * Конструктор
     */
    private function __construct() {
        
    }
    
    /**
     * Возвращает объект класса по имени проекта.
     * Если объект не существует, то создает его.
     * 
     * @param string Имя проекта
     * @return ACCache
     */
    public static function get() {
        if(self::$_instance===null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    /**
     * 
     */
    private function getConnection() {
        try {
            if(!class_exists('Memcache')) 
                return false;
            
            $memcache = new Memcache;
            $memcache->connect('127.0.0.1', 11211);
            return $memcache;
        }
        catch(Exception $e) {
            ACCache::get()->_addLibLog($e->getMessage(), 'error', __METHOD__);
            return false;
        }
    }

        
    /**
     * Запись данных в кеш
     * @param string Алиас кеша
     * @param mixed Сериализованные кешируемые данные
     * @return bool TRUE, иначе FALSE в случае ошибки 
     * 
     */
    public function write($alias, $data) {
        $cache = $this->getConnection();
        if(!$cache)
            return false;
        
        try {
            $cache->set($alias, $data, false, ACCache::get()->cachePeriod);
            $cache->close();
        }
        catch(Exception $e) {
            ACCache::get()->_addLibLog($e->getMessage(), 'error', __METHOD__);
            return false;
        }
    }
    
    /**
     * Чтение данных из кеша
     * @param string Алиас кеша
     * @return mixed Исходные данные кеша, либо FALSE в случае ошибки
     */
    public function read($alias) {
        $cache = $this->getConnection();
        if(!$cache)
            return false;
        
        try {
            $data = $cache->get($alias);
            $cache->close();
            return $data;
        }
        catch(Exception $e) {
            ACCache::get()->_addLibLog($e->getMessage(), 'error', __METHOD__);
            return false;
        }
    }

    
}

?>
