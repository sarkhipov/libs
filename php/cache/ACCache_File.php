<?php

/**
 * Класс для кеширования данных через файлы. Реализует общий интерфейс кеширования.
 * 
 * @author sarkhipov
 * @package apps3libs
 * @subpackage cache
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * 
 */
class ACCache_File implements ACCache_InterfaceCachable {
    
    private static $_instance;

    /** Директория хранения кеша */
    private $cacheDir;
    
    /**
     * Конструктор
     */
    private function __construct() {
        $this->setCacheDir(APPS3_LIB_PATH.'..'.DIRECTORY_SEPARATOR.'_cache'.DIRECTORY_SEPARATOR);
    }
    
    /**
     * Возвращает объект класса по имени проекта.
     * Если объект не существует, то создает его.
     * 
     * @param string Имя проекта
     * @return ACCache
     */
    public static function get() {
        if(self::$_instance===null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    /**
     * Проверяет существование директории кеша
     * @return bool TRUE, если существует. Иначе FALSE
     */
    private function checkCacheDirExist() {
        return file_exists($this->cacheDir);
    }

    /**
     * Создает директорию кеша
     * @return bool TRUE, иначе FALSE в случае ошибки создания
     */
    private function createCacheDir() {
        if(!$this->checkCacheDirExist()) {
            try {
                mkdir($this->cacheDir);
                return true;
            }
            catch(Exception $e) {
                ACCache::get()->_addLibLog($e->getMessage(), 'error', __METHOD__);
                return false;
            }
        }
    }
       
    /**
     * Устанавливает директорию кеширования.
     * 
     * @param string Директория
     * @return ACCache
     */
    public function setCacheDir($dir) {
        $this->cacheDir = $dir;
        $this->createCacheDir();
        return $this;
    }

    /**
     * Запись данных в кеш
     * @param string Алиас кеша
     * @param mixed Сериализованные кешируемые данные
     * @return bool TRUE, иначе FALSE в случае ошибки 
     * 
     */
    public function write($alias, $data) {
        try {
            $handle = fopen($this->cacheDir.$alias . '.cache', 'w+');
            fwrite($handle, time() . $data);
            fclose($handle);
            return true;
        }
        catch(Exception $e) {
            ACCache::get()->_addLibLog($e->getMessage(), 'error', __METHOD__);
            return false;
        }
    }
    
    /**
     * Чтение данных из кеша
     * @param string Алиас кеша
     * @return mixed Исходные данные кеша, либо FALSE в случае ошибки
     */
    public function read($alias) {
        $file = $this->cacheDir.$alias . '.cache';
        
        if(!file_exists($file)) 
            return false;
        
        if(!$this->isValidCache($file))
            return false;
        
        return substr(file_get_contents($file), 10);
    }
    
    /**
     * Проверка валидности кеша по времени
     * @param string Путь до файла
     * @return bool 
     */
    private function isValidCache($file) {
        if(file_exists($file)) {
            try {
                $handle = fopen($file, 'r');
                $cacheTime = (int) fread($handle, 10);
                $currentTime = (int) time();
                return (($currentTime - $cacheTime) < ACCache::get()->cachePeriod);
            }
            catch(Exception $e) {
                return false;
            }
        }
        return false;
    }
    
    
    
}

?>
