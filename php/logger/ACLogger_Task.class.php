<?php

/**
 * Класс для работы с Gearman-сервером для постановки задач.
 * 
 * @author sarkhipov
 * @package apps3core
 * @subpackage logger
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * 
 */
class ACLogger_Task
{
    
    /**
     * Подготавливает задачу воркера. Валидирует и обрабатывает параметры сообщения.
     * 
     * @param array Параметры сообщения
     */
    public static function add($params) {
        try {
            // валидируем набор параметров на обязательные поля и типы
            if(!ACLogger::_validateParams($params))
                return;

            // при необходимости, дополняем параметры сообщения 
            $params = ACLogger::prepareLogParams($params);
            
            // пробуем поставить задачу воркеру
            // при ошибке пробуем обработать задачу напрямую
            if(!self::_do($params)) {
                try {
                    ACLogger_Handler::handle($params);
                }
                catch(Exception $e) {
                    return false;
                }
            }
        }
        catch(Exception $e) {
            ACLogger::get()->libLog($e->getMessage(), 'error', __METHOD__);
            return false;
        }
    }
    
    /**
     * Ставит задачу воркеру.
     * @param array Массив параметров
     */
    public static function _do($params) {
        try {
            if(!class_exists('GearmanClient', false))
                return false;
            
            $gmclient = new GearmanClient();
            $gmclient->addServer('web-srv3.pro.i-free.ru', '4730');
            $taskName = 'apps3logger_handle';
            $serParams = serialize($params);

            if($params['level_number'] >=5 ) { 
                $gmclient->doHighBackground($taskName, $serParams);
            }
            else { 
                $gmclient->doBackground($taskName, $serParams);
            }
            
            return true;
        }
        catch(Exception $e) {
            ACLogger::get()->libLog($e->getMessage(), 'error', __METHOD__);
            return false;
        }
    }
    
}
?>
