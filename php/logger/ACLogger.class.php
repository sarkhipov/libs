<?php

/** 
 * Базовый класс логгера.
 * 
 * По умолчанию использует глобальное имя проекта, установленного через Apps3Load::setProject().
 * 
 * <b>Основные методы для логирования данных:</b>
 * <ul>
 *      <li>{@link log()}</li>
 *      <li></li>        
 *      <li>{@link trace()}</li>
 *      <li>{@link debug()}</li>
 * 	<li>{@link info()}</li>
 * 	<li>{@link warn()}</li>
 * 	<li>{@link error()}</li>
 * 	<li>{@link fatal()}</li>
 * </ul>
 * 
 * <br/>
 * <p><b>Примеры работы с классом:</b></p>
 * <p>
 * Лог проекта project_name с уровнем INFO:
 * <pre>
 * ACLogger::get()->setProject('project_name');
   ACLogger::get()->setMinLevel('INFO');
   ACLogger::get()->info('Message');
 * </pre> 
 * </p>
 * <br/>
 * <p>
 * Краткая запись в лог проекта, установленного через Apps3Load, с уровнем ERROR:
 * <pre>
 * ACLogger::get()->error('Message');
 * </pre> 
 * </p>
 * <br/>
 * <p>
 * Запись в лог в режиме дебага через прямой метод log:
 * <pre>
 * ACLogger::get()->debugMode()->log('Message', 'DEBUG');
 * </pre> 
 * </p>
 * 
 * @author sarkhipov
 * @package apps3libs
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * 
 */
class ACLogger 
{

    /** 
     * Имя проекта, с которым на данный момент работает логгер. 
     * Если свойство не задано, то логирование выполняться не будет. 
     */
    private $project;
    
    /** 
     * Используемый уровень логирования, если ни один не задан в ручную.
     * По умолчанию  ERROR
     */
    private $defaultLevel;
    
    /** 
     * Минимальный уровень логирования. 
     * По умолчанию WARN
     */
    private $minLevel;
    
    /** 
     * Минимальный уровень, для которого ведется трасировки файлов. 
     * По умолчанию WARN
     */
    private $minFileTraceLevel;
    
    /** 
     * Режим дебага
     * По умолчанию false
     */
    private $isDebugMode;
    
    /** Экземпляр логгера */        
    private static $_logger; 
    
    /**
     * Приватный конструктор для реализации singleton
     */
    private function __construct() {
        if(defined('APPS3_PROJECT_NAME'))
            $this->setProject(APPS3_PROJECT_NAME);
        
        $this->isDebugMode = false;
        
        $this->setDefaultLevel(ACLogger_Level::ERROR);
        $this->setMinLevel(ACLogger_Level::WARN);
        $this->setMinFileTraceLevel(ACLogger_Level::WARN);     
    }
    
    /** Приватный clone для запрета клонирования объекта */
    private function __clone(){}
    
    
    /**
     * Возвращает объект логгера по имени проекта.
     * Если объект не существует, то создает его.
     * 
     * Новому объекту задаются параметры по умолчанию:
     * <ul>
     *  <li>isDebugMode = false</li>
     *  <li>defaultLevel = 'ERROR'</li>
     *  <li>minLevel = 'WARN'</li>
     *  <li>minFileTraceLevel = 'WARN'</li>
     * </ul>
     * @param string Имя проекта 
     * @return ACLogger
     */
    public static function get($project = null) {
        if(self::$_logger===null) {
            self::$_logger = new self($project);
        }
        
        if($project)
            self::$_logger->setProject($project);
        
        return self::$_logger;
    }
    
    /**
     * Переводит работу логгера в режим дебага.
     * 
     * Все сообщения логируются с флагом 'isDebug = true'.
     * 
     * Устанавливаются новые значения для параметров:
     * <ul>
     *  <li>minLevel = 'DEBUG'</li>
     *  <li>minFileTraceLevel = 'DEBUG'</li>
     * </ul>
     * 
     * @return ACLogger
     */
    public function debugMode() {
        $this->isDebugMode = true;
        $this->setMinLevel(ACLogger_Level::DEBUG);
        $this->setMinFileTraceLevel(ACLogger_Level::DEBUG);
        return $this;
    }
    
    /**
     * Добавляет запись в лог с уровнем FATAL.
     * @param string Текст сообщения
     * @param string Алиас сообщения для группировки
     * @param string Имя проекта
     */
    public function fatal($message, $who = null, $project = null) {
        $this->log($message, 'FATAL', $who, $project);
    }
    
    /**
     * Добавляет запись в лог с уровнем ERROR.
     * @param string Текст сообщения
     * @param string Алиас сообщения для группировки
     * @param string Имя проекта
     */
    public function error($message, $who = null, $project = null) {
        $this->log($message, 'ERROR', $who, $project);
    }
    
    /**
     * Добавляет запись в лог с уровнем WARN.
     * @param string Текст сообщения
     * @param string Алиас сообщения для группировки
     * @param string Имя проекта
     */
    public function warn($message, $who = null, $project = null) {
        $this->log($message, 'WARN', $who, $project);
    }
    
    /**
     * Добавляет запись в лог с уровнем INFO.
     * @param string Текст сообщения
     * @param string Алиас сообщения для группировки
     * @param string Имя проекта
     */
    public function info($message, $who = null, $project = null) {
        $this->log($message, 'INFO', $who, $project);
    }
    
    /**
     * Добавляет запись в лог с уровнем DEBUG.
     * @param string Текст сообщения
     * @param string Алиас сообщения для группировки
     * @param string Имя проекта
     */
    public function debug($message, $who = null, $project = null) {
        $this->log($message, 'DEBUG', $who, $project);
    }
    
    /**
     * Добавляет запись в лог с уровнем TRACE.
     * @param string Текст сообщения
     * @param string Алиас сообщения для группировки
     * @param string Имя проекта
     */
    public function trace($message, $who = null, $project = null) {
        $this->log($message, 'TRACE', $who, $project);
    }

    /**
     * Добавляет запись в лог.
     * 
     * Метод подготавливает параметры записи и ставит задачу в очередь воркера.
     * @param string Текст сообщения
     * @param string Уровень логирования
     * @param string Алиас сообщения для группировки
     * @param string Имя проекта
     */
    public function log($message, $level = '', $who = null, $project = null) {
        try {
            // проверяем валидность уровня логирования
            // иначе, устанавливаем уровень логирования по умолчанию
            $level = strtoupper($level);
            if(!$level || !array_key_exists($level, ACLogger_Level::levels()))
                $level = $this->defaultLevel;

            // если уровень логирования ниже минимального
            // выходим из метода
            if(ACLogger_Level::getLevelByString($level) < $this->minLevel)
                return;

            // если не установлен проект
            // выходим из метода
            if(!$this->project && !$project)
                return;
            
            // если уровень использования трасировки ниже минимального
            // то не используем ее
            $trace = $this->prepareTrace(debug_backtrace());
            if(ACLogger_Level::getLevelByString($level) < $this->minFileTraceLevel) {
                $trace = null;
            }

            // если передано имя проекта, то используем его
            $projectName = ($project)?$project:$this->project;

            // соберем все параметры сообщения
            $itemParams = array(
                'message' => $message,
                'level' => $level,
                'level_number' => ACLogger_Level::getLevelByString($level),
                'project' => $projectName,
                'is_debug' => $this->isDebugMode,
                'file_trace' => $trace,
                'who' => $who,
                'date_original'=> date('Y-m-d H:i:s')
            );

            // добавляем задачу в gearman
            ACLogger_Task::add($itemParams);
            
        }
        catch(Exception $e) {
            $this->libLog($e->getMessage(), 'error', __METHOD__);
        }
    }
    
    /**
     * Подготавливает параметры сообщения перед записью
     * 
     * @param array Параметры записи
     */
    public static function prepareLogParams($params) {
        
        if(!isset($params['level_number']) || !is_int($params['level_number']))
            $params['level_number'] = ACLogger_Level::getLevelByString($params['level']);
        
        if(!isset($params['is_debug']) || $params['is_debug']===null)
            $params['is_debug'] = false;
        
        if(!isset($params['date_original']) || $params['date_original']===null)
            $params['date_original'] = date('Y-m-d H:i:s');
      
        return $params;
    }
    
    /**
     * Валидация параметров сообщения перед записью в базу.
     * 
     * @param array Параметры записи
     */
    public static function _validateParams($params) {
        $reqParams = array('project', 'message', 'level');
        foreach ($reqParams as $param) {
            if(!isset($params[$param]) || empty($params[$param])) {
                return false;
            }
        }
        return true;
    }
    
    
    /**
     * Устанавливает текущее имя проекта.
     * 
     * @param string Имя проекта
     * @return ACLogger
     */
    public function setProject($project) {
        $this->project = $project;
        return $this;
    }

    /**
     * Устанавливает уровень логирования по умолчанию.
     * 
     * @param string Уровень логирования
     * @return ACLogger
     */
    public function setDefaultLevel($level) {
        if(!is_int($level)) {
            $level = ACLogger_Level::getLevelByString($level);
        }
        
        $this->defaultLevel = $level;
        
        return $this;
    }
    /**
     * Устанавливает минимальный уровень логирования.
     * 
     * @param string Уровень логирования
     * @return ACLogger
     */
    public function setMinLevel($level) {
        if(!is_int($level)) {
            $level = ACLogger_Level::getLevelByString($level);
        }
        
        $this->minLevel = $level;
        
        return $this;
    }
    
    /**
     * Устанавливает минимальный уровень, для которого сохраняется трасировка файлов.
     * 
     * @param string Уровень логирования
     * @return ACLogger
     */
    public function setMinFileTraceLevel($level) {
        if(!is_int($level)) {
            $level = ACLogger_Level::getLevelByString($level);
        }
        
        $this->minFileTraceLevel = $level;
        
        return $this;
    }
    
    
    /**
     * Обрабатывает массив трасировки файлов. 
     * 
     * <b>Массив должен быть получен функцией debug_backtrace или иметь аналогичный формат.</b>
     * 
     * @param array Массив трасировки
     * @param bool Результат в виде строки (иначе массив)
     * @return mixed
     */
    private function prepareTrace($trace, $inString = true) {
        if(!is_array($trace) || empty($trace)) {
            return null;
        }
        
        $traceArray = array();
        $fields = array('file', 'line', 'function', 'class');
        
        $_trace = array_reverse($trace);
        // do not reverse for yii
        if(isset($_trace[0]) && (
           (isset($_trace[0]['class']) && $_trace[0]['class']==='CApplication' && 
            isset($_trace[0]['function']) && $_trace[0]['function']==='run') ||
           (strpos($_trace[0]['file'], 'yiic')!==false)     
           )) {
            $_trace = $trace; 
        }
        
        $trace = array_slice($_trace, 0, 3);
        
        foreach ($trace as $file) {
            $traceArrayFile = array();
            foreach($fields as $field) {
                if(isset($file[$field]) && $file[$field])
                    $traceArrayFile[$field] = $file[$field];
            }
            
            $traceArray[] = $traceArrayFile;
        } 
        
        if($inString)
            $traceArray = json_encode($traceArray);
        
        return $traceArray;
    }

    /**
     * Внутренний лог работы класса.
     * 
     * @param string Текст сообщения
     * @param string Тип сообщения
     * @param string Алиас 
     */
    public function libLog($message, $level = 'error', $who = '') {
        try {
            $folder = APPS3_LIB_PATH.'logger/log/';
            if(file_exists($folder)) {
                $handle = fopen($folder . $level . '_' . date('Y-m-d') . '.log', 'a+');
                fwrite($handle, date('Y-m-d H:i:s') . ' : ' . $who . ' : ' . $message . "\r\n");
                fclose($handle);
            }
            else {
                if($level==='error')
                    echo '<span style="color: red;">ACLogger::Error in {'.$who.'}. '.$message.'</span>';
            }
        }
        catch(Exception $e) {
            return false;
        }
    }

}


?>
