<?php

/**
 * Класс для обработки задач вокера.
 * 
 * Подключается только на площадке apps3.
 * 
 * @author sarkhipov
 * @package apps3core
 * @subpackage logger
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 */
class ACLogger_Handler
{
    
    /**
     * Конструктор класса
     */
    public function __construct() {
        
    }

    /**
     * Обработка задачи воркера. Отправляет нотификацию, пишет сообщение в базу.
     * @param string Сериализованный массив параметров 
     */
    public function handle($job) {

        if(class_exists('GearmanClient', false)) {
            $params = unserialize($job->workload());
        }
        else {
            $params = $job;
        }
        
        // пишем сообщение в базу
        $newParams = ACLogger_DB::get()->add($params);
        if(!$newParams) 
            $newParams = $params;

        // отправляем нотификацию
        ACLogger_Notify::handle($newParams);
        
    } 
}
?>
