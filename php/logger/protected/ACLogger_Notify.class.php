<?php

/**
 * Класс для работы с оповещениями.
 * 
 * Подключается только на площадке apps3.
 * 
 * @author sarkhipov
 * @package apps3core
 * @subpackage logger
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 */
class ACLogger_Notify
{
    /**
     * Обработка необходимых нотификаций сообщения логгера.
     * 
     * @param array Массив параметров 
     */
    public static function handle($params) {
        if($params['level_number'] >= 5) {
            $notify = self::prepareMail($params);
            self::send($notify['address'], $notify['subject'], $notify['message']);
        }
            
    }
    
    /**
     * Подготавливает email-нотификацию к отправке.
     * @param array Массив параметров
     * @return array
     */
    public static function prepareMail($params) {
        $result = array();
        $file_trace = ($params['file_trace'])?json_decode($params['file_trace'], true):array();
        $result['address'] = (isset($params['notify']) && $params['notify'])? explode(',', $params['notify']):array();
        $result['subject'] = $params['level'].' level on project "'.$params['project'].'"';
        $result['message'] = '<p><a href="http://apps3.mmi.ru/monitor/site/logs?LogMessage[id]='.$params['log_id'].'">View in the Apps3 Monitor</a></p>'.
                             '<p><strong>Log message ID: </strong>'.$params['log_id'].'</p>'.
                             '<p><strong>Message: </strong>'.$params['message'].'</p>'.
                             '<strong>File Trace: </strong><pre>'.print_r($file_trace, true).'</pre>';   
        return $result;
    }

    /**
     * Отправляет email-сообщение.
     * 
     * @param mixed Адрес получателей. Строка или массив адресов.
     * @param string Тема сообщения
     * @param string Текст сообщения
     * @return bool TRUE, либо FALSE в случае ошибки.
     */
    public static function send($address, $subject, $message) {
        try {
            include_once APPS3_LIB_PATH.'phpmailer/class.phpmailer.php';
            include_once APPS3_LIB_PATH.'phpmailer/class.smtp.php';

            $mail = new PHPMailer();
            $mail->IsSMTP(); 
            $mail->Host = 'mail.pro.i-free.ru';
            $mail->SMTPDebug = 0;                     
            $mail->SetFrom('no-reply@apps3.mmi.ru', 'Apps3 Logger');

            $mail->Subject = $subject;
            $mail->MsgHTML($message);
            
            if(!is_array($address))
                $address = array($address);
            foreach ($address as $item)
                $mail->AddAddress($item);

            if(!$mail->Send())
                return false;

            return true;
        }
        catch(Exception $e) {
            ACLogger::get()->libLog($e->getMessage(), 'error', __METHOD__);
            return false;
        }
    }
}
?>
