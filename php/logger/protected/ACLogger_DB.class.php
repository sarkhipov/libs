<?php

/**
 * Класс для работы с базой данных логгера.
 * 
 * Подключается только на площадке apps3.
 * 
 * @author sarkhipov
 * @package apps3core
 * @subpackage logger
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * 
 */
class ACLogger_DB {

    /** Экземпляр PDO-класса */
    private $_db;

    /** Экземпляр класса */
    private static $_instance;

    /**
     * Приватный конструктор для реализации singleton
     */
    private function __construct() {
        //$this->_db = new PDO("pgsql:dbname=log;host=localhost", "some_user", "some_pass");
        
    }

    /** Приватный clone для запрета клонирования объекта */
    private function __clone() {
        
    }

    /**
     * Возвращает экземпляр класса.
     * @return ACLogger_DB
     */
    public static function get() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Добавляет запись в базу данных.
     * @param array Массив полей
     * @return mixed Массив параметров записи лога, либо FALSE в случае ошибки
     */
    public function add($params) {

        try {
            $project = $this->createProject($params['project']);
            
            $dbQ = $this->_db->prepare("
                INSERT INTO log_messages
                (project, level, level_number, is_debug, file_trace, message, who, date_original)
                VALUES (:project, :level, :level_number, :is_debug, :file_trace, :message, :who, :date_original)
            ");
            
            $dbQ->bindParam(':project', $params['project']);
            $dbQ->bindParam(':level', $params['level']);
            $dbQ->bindParam(':level_number', $params['level_number']);
            $dbQ->bindParam(':is_debug', $params['is_debug'], PDO::PARAM_BOOL);
            $dbQ->bindParam(':file_trace', $params['file_trace']);
            $dbQ->bindParam(':message', $params['message']);
            $dbQ->bindParam(':who', $params['who']);
            $dbQ->bindParam(':date_original', $params['date_original']);
            
            $dbQ->execute();
            
            if($dbQ->errorCode()!='00000') {
                ACLogger::get()->libLog(json_encode($dbQ->errorInfo()), 'error', __METHOD__);
                return false;
            }
            
            $params['log_id'] = $this->_db->lastInsertId('messages_id_seq');
            $params['notify'] = ($project && isset($project['notify']))?$project['notify']:'';
            return $params;
        }
        catch(Exception $e) {
            ACLogger::get()->libLog($e->getMessage(), 'error', __METHOD__);
            return false;
        }
    }
    
    /**
     * Проверяет на существование проект
     * @param string Имя проекта
     * @return mixed
     */
    private function isProjectExist($project) {
        
        try {
            $dbQ = $this->_db->prepare("SELECT * FROM log_projects WHERE project=:project");  
            $dbQ->bindParam(":project", $project);
            $dbQ->execute();
            $result = $dbQ->fetch(PDO::FETCH_ASSOC);

            if(!isset($result["project"])) 
                return false;

            return $result;
        }     
        catch(Exception $e) {
            ACLogger::get()->libLog($e->getMessage(), 'error', __METHOD__);
            return false;
        }
    }
    
    /**
     * Создает новый проект 
     * @param string Имя проекта
     * @return mixed 
     */
    private function createProject($project) {
        try {
            $existingPproject = $this->isProjectExist($project);
            if($existingPproject)
                return $existingPproject;
            
            $dbQ = $this->_db->prepare("
                INSERT INTO log_projects
                (project) VALUES (:project)
            ");
            
            $dbQ->bindParam(':project', $project);
            $dbQ->execute();
            
            if($dbQ->errorCode()!='00000') {
                ACLogger::get()->libLog(json_encode($dbQ->errorInfo()), 'error', __METHOD__);
                return false;
            }
            
            return true;
        }
        catch(Exception $e) {
            ACLogger::get()->libLog($e->getMessage(), 'error', __METHOD__);
            return false;
        }
    }
}

?>
