<?php

/**
 * Класс для работы с уровнями логирования.
 * 
 * @author sarkhipov
 * @package apps3core
 * @subpackage logger
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * 
 */
class ACLogger_Level 
{
    const FATAL = 6;
    const ERROR = 5;
    const WARN = 4;
    const INFO = 3;
    const DEBUG = 2;
    const TRACE = 1;
    
    
    /**
     * Возвращает числовой уровень логирования по строке
     * 
     * @param string Название уровня
     * @return int 
     */
    public static function getLevelByString($string) {
        $levels = self::levels();
        try {
            return $levels[$string];
        }
        catch(Exception $e) {
            return self::INFO;
        }
    }
    
    /**
     * Возвращает строковое название уровня по числовому значению
     * 
     * @param int Уровень
     * @return string
     */
    public static function getStringByLevel($level) {
        $levels = self::levels();
        foreach ($levels as $name=>$_level) {
            if($_level == $level)
                return $name;
        }
        return 'INFO';
    }
    
    
    /**
     * Возвращает список уровней логирования
     * @return array
     */
    public static function levels() {
        return array(
            'FATAL' => self::FATAL,
            'ERROR' => self::ERROR,
            'WARN' => self::WARN,
            'INFO' => self::INFO,
            'DEBUG' => self::DEBUG,
            'TRACE' => self::TRACE
        );
    }
    
}
?>
