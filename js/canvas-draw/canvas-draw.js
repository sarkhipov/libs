/**
 * @file A little library for help in 2d canvas drawing. Provide you to work with draw elements like with objects. Packaged in module {@link module:canvas-draw}
 * @author sarkhipov
 */

/**
 * <h2>Usage examples</h2>
 * <h4>Basics</h4>
 * <p>First we need to initialize our library with canvas element:</p>
 * <pre>cd.init(canvasID);</pre>
 * <p>Next, we start drawing process. You can set fps value for drawing speed:</p>
 * <pre>cd.start(fps);</pre>
 * <p>And now you can start draw. For example, lets draw simple red circle:</p>
 * <pre>var circle = cd.circle(100, 100, 50, "red");</pre>
 * <p>We can bind click event for our circle. In the event callback lets change circle color:</p>
 * <pre>
 * circle.bind('click', function(item) {
 *      item.set({color: "green"});
 * });
 * </pre>
 * <p>Or we can animate circle. Lets move it to the right and then remove:</p>
 * <pre>
 * circle.animate({'x': 300}, 3000, function() {
 *      circle.remove();
 * });
 * </pre>
 * <h4>Prepare static files</h4>
 * <p>You can preload static files before use it in library. After they loaded, you can start drawing.
 * <pre>
 * cd.init('canvas');
 * cd.loadFiles([
 *      {'type': 'audio', 'name': 'sound', 'src': 'sound2.mp3'},
 *      {'type': 'image', 'name': 'image', 'src': 'i/lawn.jpg'},
 *  ],
 *  function() {
 *      cd.start();
 *      cd.image(cd.getImage('image'), 100, 100);
 *  }
 * );
 * </pre>
 * <h2>Avaliable elements</h2>
 * <p>
 *     With library you can draw line, circle, circle,
 *     rect, image and text.
 * </p>
 * <h2>Element methods</h2>
 * <p>
 *      You can get or set elements properties, bind events, animate (or create animate queue) or remove element.
 * </p>
 * @module canvas-draw
 */

/**
 * @namespace
 */
var cd = {
    /**
     * HTML canvas object
     */
    canvas: null,
    /**
     * Current 2d context
     */
    context: null,
    // collection of existing elements
    list: [],
    listMap: {},
    // use propagation effect in events
    propagation: true,

    // interaction events
    isTouch: false,
    _click: 'click',

    // library loaded flag
    loaded: false,
    // common library process object
    process: null,
    fps: 60,

    // static files objects
    files: {},
    filesLoaded: 0,

    /**
     * Start point to use library.
     * Init canvas element and 2d context by id.
     * Prepare main events and init library elements.
     * @param canvasId {string} ID of the HTML canvas element
     * @returns {boolean}
     */
    init: function(canvasId) {
        if(this.loaded) {
            return true;
        }
        // first lets init canvas element by id
        try {
            var canvas = document.getElementById(canvasId);
            if(canvas.tagName.toUpperCase()!=='CANVAS') {
                this.__log('element is not a canvas', 'error');
                return false;
            }
            this.canvas = canvas;
        }
        catch(e) {
            this.__log('canvas element not found by canvasId', 'error');
            return false;
        }

        // then lets create 2d context
        try {
            this.context = this.canvas.getContext('2d');

            // handle some events on canvas
            this.__initEvents();
            this.canvas.addEventListener(this._click, function(e){ cd.__canvasEvent('click', e);});

            // declare library elements
            this.__declareElements();

            this.loaded = false;
        }
        catch(e) {
            this.__log('canvas 2d context not supported', 'error');
        }
    },

    /**
     * Start common library process
     * @param fps {int} Number or frames per second on drawing process. 60 by default.
     */
    start: function(fps) {
        if(fps) {
            this.fps = parseInt(fps);
        }
        cd.__draw();
        this.process = setInterval(function() {
            cd.__draw();
        }, 1000/this.fps);
    },

    /**
     * Stop common process
     */
    stop: function() {
        clearInterval(this.process);
        this.process = null;
    },

    /*******************
     ** LIBRARY ELEMENTS
     ******************/

    /**
     * Elements constructor
     * @param type {string} Element type
     * @param options {object} Set of element params
     * @constructor
     */
    canvas_element: function(type, options) {
        this.type = type;
        this.options = options;
        this.id = 'cd'+parseInt(Math.random()*100000);
        this.events = {};
        this.animating = {};
        this.queueList = [];
        cd.__addInList(this);
    },

    /**
     * Declare elements methods
     * @private
     */
    __declareElements: function() {
        /**
         * Bind event for element
         * @param event {string} Event name
         * @param callback {function} Callback method. Library provide you to use in this function target element as the first argument.
         * @param params {array} Set of params, that will be accessible in the callback function (begins from second argument).
         */
        cd.canvas_element.prototype.bind = function(event, callback, params) {
            var events = this['events'];
            if(!events[event]) {
                events[event] = [];
            }
            if(!params) {
                params = [];
            }
            events[event].push({'callback': callback, 'params': params});
        };

        /**
         * Get element property by name
         * @param option {string} Param name
         * @returns {*}
         */
        cd.canvas_element.prototype.get = function(option) {
            if(this['options'][option]) {
                return this['options'][option];
            }
            return false;
        };
        /**
         * Set element properties
         * @param option {*} Param name, or key-value object
         * @param value {string} Param new value (used if option is string)
         * @returns {boolean}
         */
        cd.canvas_element.prototype.set = function(option, value) {
            // if we have an option as an object, set each option param as string
            if(typeof option === 'object') {
               for(key in option) {
                   this.set(key, option[key]);
               }
            }
            if(this['options'][option]) {
                this['options'][option] = value;
                return true;
            }
            return false;
        };

        /**
         * Remove element
         * @returns {boolean}
         */
        cd.canvas_element.prototype.remove = function() {
            delete cd.list[cd.listMap[this.id]];
            delete cd.listMap[this.id];
            return true;
        };

        /**
         * Animate element
         * @param o {object} Set of params that we need to animate
         * @param duration {int} Duration speed in milliseconds
         * @param callback {function} Callback function
         * @returns {boolean}
         */
        cd.canvas_element.prototype.animate = function(o, duration, callback) {
            // allow only one animation
            if(this.animating.is) {
                return false;
            }
            duration = duration || 0;
            this.animating.is = true;
            // calculate step for each option
            this.animating.step = {};
            this.animating.direction = {};
            for(var key in o) {
                this.animating.step[key] = (o[key] - this.get(key)) / (duration / (1000/cd.fps));
                this.animating.direction[key] = (o[key] > this.get(key))?1:-1;
            }
            // run animate process
            this.animating.process = setInterval(function(elem) {
                for(var key in elem.animating.step) {
                    elem.set(key, elem.get(key) + elem.animating.step[key]);
                    if((elem.get(key) - o[key])*elem.animating.direction[key] >= 0) {
                        clearInterval(elem.animating.process);
                        elem.animating = {};
                        if(callback) {
                            callback();
                        }
                        // check queue for this element and run next function
                        if(elem.queueList[0]) {
                            elem.queueList.splice(0, 1);
                            if(elem.queueList[0]) {
                                elem.queueList[0]();
                            }
                        }
                        break;
                    }
                }
            }, 1000/cd.fps, this);
        };

        /**
         * Create animating queue and execute each one. You can use one or more argumnets to describe animations.
         * @param function {function} Function, where you call animate function.
         * @returns {boolean}
         */
        cd.canvas_element.prototype.queue = function() {
            for(var i in arguments) {
                this.queueList.push(arguments[i]);
            }
            if(arguments[0]) {
                arguments[0]();
            }
            return true;
        }
    },

    /**
     * Add created element in global list for next usage
     * @param element
     * @private
     * @returns {boolean}
     */
    __addInList: function(element) {
        this.list.push(element);
        this.listMap[element.id] = this.list.length -1;
        return true;
    },

    /**
     * Return library element by its id
     * @param id {string} Element id
     * @returns {*}
     */
    getElementById: function(id) {
        try {
            return this.list[this.listMap[id]];
        }
        catch(e) {
            return false;
        }
    },

    /**
     * Create simple rect
     * @param x {float} X coordinate
     * @param y {float} Y coordinate
     * @param w {float} Rect width
     * @param h {float} Rect height
     * @param color {string} Color
     * @param width {float} Border width
     * @param strokeColor {string} Border color
     * @returns {cd.canvas_element}
     */
    rect: function(x, y, w, h, color, width, strokeColor) {
        return new this.canvas_element('rect', {
            'x': x, 'y': y, 'w': w, 'h': h,
            'width': width, 'color': color, 'strokeColor': strokeColor
        });
    },

    /**
     * Create simple line
     * @param fromX {float} First point X coordinate
     * @param fromY {float} First point Y coordinate
     * @param toX {float} Second point X coordinate
     * @param toY {float} Second point Y coordinate
     * @param color {string} Color
     * @param width {float }Border width
     * @param cap {string} Line cap style. See html5 canvas doc for details
     * @returns {cd.canvas_element}
     */
    line: function(fromX, fromY, toX, toY, color, width, cap) {
        return new this.canvas_element('line', {
            'x1': fromX, 'y1': fromY, 'x2': toX, 'y2': toY,
            'w': width, 'color': color, 'cap': cap
        });
    },

    /**
     * Create simple ellipse
     * @param x {float} X coordinate
     * @param y {float} Y coordinate
     * @param a {float} X-radius size
     * @param b {float} Y-radius size
     * @param color {string} Color
     * @param width {float} Border width
     * @param strokeColor {string} Border color
     * @returns {cd.canvas_element}
     */
    ellipse: function(x, y, a, b, color, width, strokeColor) {
        return new this.canvas_element('ellipse', {
            'x': x, 'y': y, 'a': a, 'b': b,
            'r': ((a==b)?a:null),
            'w': width, 'color': color, strokeColor: strokeColor
        });
    },

    /**
     * Create simple circle
     * @param x {float} X coordinate
     * @param y {float} Y coordinate
     * @param r {float} Radius
     * @param color {string} Color
     * @param width {float} Border width
     * @param strokeColor  {string} Border color
     * @returns {cd.canvas_element}
     */
    circle: function(x, y, r, color, width, strokeColor) {
        return this.ellipse(x, y, r, r, color, width, strokeColor);
    },

    /**
     * Create simple image
     * @param image
     * @param x {float} X coordinate
     * @param y {float} Y coordinate
     * @param w {float} Width
     * @param h {float} Height
     * @param cx {float} Clip X coordinate
     * @param cy {float} Clip Y coordinate
     * @param cw {float} Clip width
     * @param ch {float} Clip height
     * @returns {cd.canvas_element}
     */
    image: function(image, x, y, w, h, cx, cy, cw, ch) {
        return new this.canvas_element('image', {
            'x': x, 'y':y, 'w': w, 'h': h,
            'cx': cx, 'cy':cy, 'cw': cw, 'ch': ch,
            'image': image
        });
    },

    /**
     * Create simple text
     * @param x {float} X coordinate
     * @param y {float} Y coordinate
     * @param text {string} Text
     * @param color {string} Color
     * @param font {string} Text font
     * @param width {float} Border width
     * @param strokeColor Border color
     * @returns {cd.canvas_element}
     */
    text: function(x, y, text, color, font, width, strokeColor) {
        var measure = this.context.measureText(text);
        var rE = /(\d)+px/ig;
        var reRes = rE.exec(font);
        var height = ((reRes[0])?reRes[0].replace('px', ''):'20')*0.66;
        return new this.canvas_element('text', {
            'x': x, 'y': y-height, 'w': measure.width, 'h': height,
            'color': color, 'font': font, 'width': width, 'strokeColor': strokeColor,
            'text': text
        });
    },

    /*******************
     ** DRAW METHODS
     ******************/

    /**
     * Main method for drawing.
     * Draw all created canvas_elements
     * @private
     */
    __draw: function() {
        // first clear context
        this.clear();
        // then draw each canvas_element in collection
        for(var i in this.list) {
            var element = this.list[i];
            // upper first letter in type, to call draw method
            var elementType = element['type'].charAt(0).toUpperCase() + element['type'].slice(1);
            try {
                this['__draw'+elementType](element.options);
            }
            catch(e){}
        }
    },

    /**
     * Simple rectangle
     * @param o
     * @private
     * @returns {boolean}
     */
    __drawRect: function(o) {
        try {
            this.context.beginPath();
            this.context.rect(o.x, o.y, o.w, o.h);

            if(o.color) {
                this.context.fillStyle = o.color;
            }
            this.context.fill();

            if(o.strokeColor) {
                this.context.strokeStyle = o.strokeColor;
            }
            if(o.width) {
                this.context.lineWidth = o.width;
                this.context.stroke();
            }
            return true;
        }
        catch(e) {
            this.__log('rect draw error - '+ e, 'error');
            return false;
        }
    },

    /**
     * Simple line
     * @param o
     * @private
     * @returns {boolean}
     */
    __drawLine: function(o) {
        try {
            this.context.beginPath();
            this.context.moveTo(o.fromX, o.fromY);
            this.context.lineTo(o.toX, o.toY);
            if(o.color){
                this.context.strokeStyle = o.color;
            }
            if(o.width) {
                this.context.lineWidth = o.width;
            }
            if(o.cap) {
                this.context.lineCap = o.cap;
            }
            this.context.stroke();
            return true;
        }
        catch(e) {
            this.__log('line draw error - '+ e, 'error');
            return false;
        }
    },

    /**
     * Simple circle
     * @param o
     * @private
     * @returns {boolean}
     */
    __drawCircle: function(o) {
        try {
            o.a = o.r;
            o.b = o.r;
            return this.__drawEllipse(o);
        }
        catch(e) {
            this.__log('circle draw error - ' + e, 'error');
        }
    },

    /**
     * Simple ellipse
     * @param o
     * @private
     * @returns {boolean}
     */
    __drawEllipse: function(o) {
        try {
            if(o.r) {
                o.a = o.r;
                o.b = o.r;
            }
            this.context.save();
            this.context.beginPath();
            this.context.translate(o.x- o.a, o.y- o.b);
            this.context.scale(o.a, o.b);
            this.context.arc(1, 1, 1, 0, 2 * Math.PI, false);
            this.context.restore();
            if(o.color) {
                this.context.fillStyle = o.color;
                this.context.fill();
            }
            if(o.strokeColor) {
                this.context.strokeStyle = o.strokeColor;
            }
            if(o.w) {
                this.context.lineWidth = o.w;
                this.context.stroke();
            }
            return true;
        }
        catch(e) {
            this.__log('ellipse draw error - ' + e, 'error');
            return false;
        }
    },

    /**
     * Simple image
     * Use only loaded Image() object
     * @param o
     * @private
     * @returns {boolean}
     */
    __drawImage: function(o) {
        try {
            if(!o.cx) {
                if(!o.w) o.w = o.image.width;
                if(!o.h) o.h = o.image.height;
                this.context.drawImage(o.image, o.x, o.y, o.w, o.h);
            }
            else {
                this.context.drawImage(o.image, o.cx, o.cy, o.cw, o.ch, o.x, o.y, o.w, o.h);
            }
            return true;
        }
        catch(e) {
            this.__log('image draw error - ' + e, 'error');
            return false;
        }
    },

    /**
     * Simple text message
     * @param o
     * @private
     * @returns {boolean}
     */
    __drawText: function(o) {
        try {
            if(o.color) {
                this.context.fillStyle = o.color;
            }
            if(o.font) {
                this.context.font = o.font;
            }
            this.context.fillText(o.text, o.x, o.y);

            if(o.strokeColor) {
                this.context.strokeStyle = o.strokeColor;
            }
            if(o.width) {
                this.context.lineWidth = o.width;
                this.context.strokeText(o.text, o.x, o.y);
            }
            return true;
        }
        catch(e) {
            this.__log('text draw error - ' + e, 'error');
            return false;
        }
    },


    /*******************
     ** EVENTS
     ******************/

    /**
     * Set touch/click events before interaction
     * @private
     * @returns {boolean}
     */
    __initEvents: function() {
        if(typeof(document.ontouchstart)!="undefined") {
            this.isTouch = true;
            this._click = 'touchstart';
        }
        return true;
    },

    /**
     * When main canvas has triggered event
     *
     * @param event
     * @param e
     * @private
     */
    __canvasEvent: function(event, e) {
        // if we have an event, execute all callbacks
        this.propagation = true;

        var list = this.list.reverse();
        for(var key in list) {
            var elem = list[key];

            // check if we have binds for this element and event
            if(elem['events'][event]) {
                var type = elem['type'];
                var isTriggered = false;

                // check click
                if (event === 'click' || event === 'dblclick') {
                    var x = (!this.isTouch)?e.offsetX: e.touches[0].pageX - this.canvas.offsetLeft;
                    var y = (!this.isTouch)?e.offsetY: e.touches[0].pageY - this.canvas.offsetTop;

                    // pass by all element types and check each one
                    if (type === 'line') {
                        isTriggered = this.__isLineClicked(x, y, elem['options']);
                    }
                    if (type === 'rect' || type === 'image' || type === 'text') {
                        isTriggered = this.__isRectClicked(x, y, elem['options']);
                    }
                    if (type === 'ellipse') {
                        isTriggered = this.__isEllipseClicked(x, y, elem['options']);
                    }
                }

                // if we have triggered event on the element
                if(isTriggered) {
                    list = this.list.reverse();

                    for(i in elem['events'][event]) {
                        // include element as the first callback param
                        elem['events'][event][i]['params'].unshift(elem);
                        // call callback with params
                        elem['events'][event][i]['callback'].apply(null, elem['events'][event][i]['params']);
                    }

                    list = this.list.reverse();
                    // if propagation stopped, exec only callbacks fo first elem
                    if(!this.propagation) {
                        break;
                    }
                }
            }
        }

        // revert list in normal state
        list = this.list.reverse();
    },

    /**
     * Check if we click simple line
     * @param x
     * @param y
     * @param o
     * @private
     * @returns {boolean}
     */
    __isLineClicked: function(x, y, o) {
        var coords = {};
        if(!o.w) o.w = 0;
        // if it something like x = const
        if(o.x1 == o.x2) {
            coords = { 'x1': o.x1- o.w/2, 'x2': o.x1+ o.w/2, 'y1': Math.min(o.y1, o.y2), 'y2': Math.max(o.y1, o.y2) }
            return this.__isAreaClicked(x,y,coords);
        }
        // if it is something like y = const
        if(o.y1 == o.y2) {
            coords = {'x1': Math.min(o.x1, o.x2), 'x2': Math.max(o.x1, o.x2), 'y1': o.y1- o.w/2, 'y2': o.y2+ o.w/2 }
            return this.__isAreaClicked(x,y,coords);
        }
        // if it is something like y = kx + b
        var diff = ((x - o.x1)/(o.x1 - o.x2)) - ((y - o.y1)/(o.y1 - o.y2));
        coords = {'x1': Math.min(o.x1, o.x2), 'x2': Math.max(o.x1, o.x2), 'y1': Math.min(o.y1, o.y2), 'y2': Math.max(o.y1, o.y2)}
        if(Math.abs(diff) < o.w/200) {
            return this.__isAreaClicked(x,y,coords);
        }
        return false;
    },

    /**
     * Check if we click simple rect
     * @param x
     * @param y
     * @param o
     * @private
     * @returns {boolean}
     */
    __isRectClicked: function(x, y, o) {
        if(!o.width) o.width = 0;
        var coords = {
            'x1': o.x - o.width/2,
            'y1': o.y - o.width/2,
            'x2': o.x + o.w + o.width/2,
            'y2': o.y + o.h + o.width/2
        }
        return this.__isAreaClicked(x,y,coords);
    },

    /**
     * Check if we click at simple ellipse
     * @param x
     * @param y
     * @param o
     * @private
     * @returns {boolean}
     */
    __isEllipseClicked: function(x, y, o) {
        if(!o.w) o.w = 0;
        var dist = (Math.pow(x - o.x, 2) / Math.pow(o.a + o.w/2, 2)) + (Math.pow(y - o.y,2) / Math.pow(o.b + o.w/2, 2));
        return (dist <= 1);
    },

    /**
     * Check if we click on square area
     *
     * @param x
     * @param y
     * @param o
     * @private
     * @returns {boolean}
     */
    __isAreaClicked: function(x, y, o) {
        return (x > o.x1 && x < o.x2 && y > o.y1 && y < o.y2);
    },

    /**
     * Stop propagation for current event.
     * Need to be used in event callback
     */
    stopPropagation: function() {
        cd.propagation = false;
    },


    /*******************
     ** STATIC FILES
     ******************/
    /**
     * Load static files
     * @param files {array} Set of static files. Each file have keys 'name', 'type', 'src'. Avaliable types - image and audio.
     * @param callback {function} Callback function after all files loading
     */
    loadFiles: function(files, callback) {
        // try to load every static element from list
        for(var i in files) {
            try {
                // create file object
                var file = files[i];
                var fileObj = null;
                var loadEvent = null;
                if(file.type === 'image') {
                    fileObj = new Image();
                    loadEvent = 'load';
                }
                else if (file.type === 'audio') {
                    fileObj = new Audio();
                    loadEvent = 'canplaythrough';
                }
                fileObj.src = file.src;

                // put new object into files list
                this.files[file.name] = fileObj;
                // start to load file and wait callback
                fileObj.addEventListener(loadEvent, function() {
                    cd.filesLoaded++;
                    if(cd.filesLoaded >= Object.keys(cd.files).length) {
                        callback();
                    };
                });
            }
            catch(e) {
                this.__log('file load error: '+ e, 'error');
            }
        }
    },

    /**
     * Get static file by name
     * @param name {string} File name
     * @returns {*}
     */
    getFile: function(name) {
        if(this.files[name]) {
            return this.files[name];
        }
        return false;
    },

    /*******************
     ** SYSTEM METHODS
     ******************/

    /**
     * Clear context
     */
    clear: function() {
        this.context.save();
        this.context.setTransform(1, 0, 0, 1, 0, 0);
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.restore();
    },

    /**
     * Library log, use console by default
     *
     * @param message
     * @param level
     * @private
     */
    __log: function(message, level) {
        message = 'Canvas Draw: ' + message;
        switch(level) {
            case 'info':
                console.info(message);
                break;
            case 'error':
                console.error(message);
                break;
            default:
                console.log(message);
        }
    }
}