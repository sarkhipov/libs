
// Make previews and put them to attachments container
var attachment = {
    add: function(url) {
        var html = '<div class="attache-image"  title="Удалить фото" onclick="attachment.remove(this)"><img src="'+url+'" /></div>';
        document.getElementById('attachments').innerHTML += html;
    },

    remove: function(elememt) {
        elememt.remove();
    }
}

// Work with file reading - input file & drag'n'drop
var files = {
    run: function() {
        files.view.draw();
        files.view.bind();
    },

    read: function(files) {
        for (var i = 0, f; f = files[i]; i++) {
            if (!f.type.match('image.*')) continue;
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e) {
                    attachment.add(e.target.result);
                };
            })(f);
            reader.readAsDataURL(f);
        }
    },

    view: {
        fileInput: '',
        chooseFile: '',
        dropBox: '',

        draw: function() {
            document.getElementById('files').innerHTML = '' +
                '<input type="file" name="file" id="fileInput" multiple="true" hidden="hidden" />' +
                '<a href="javascript:void(0)" id="chooseFile" class="btn">Выберите файлы</a>' + ' или ' +
                '<div id="dropFile">тащите сюда</div>' + ' или '+
                '<a class="btn" id="show-camera"><i class="icon-camera"></i></a>';

            this.fileInput = document.getElementById('fileInput');
            this.chooseFile = document.getElementById('chooseFile');
            this.dropBox = document.getElementById('dropFile');
        },

        bind: function() {
            this.chooseFile.onclick = function() {
                var evt = document.createEvent("HTMLEvents");
                evt.initEvent("click", false, false);
                files.view.fileInput.dispatchEvent(evt);
            }
            files.view.fileInput.onchange = function(e) {
                files.read(e.target.files);
            };
            this.dropBox.ondragover = function(e) {
                e.preventDefault();
                files.view.dropBox.classList.add('dragged');
                return false;
            }
            this.dropBox.ondragleave = function(e) {
                e.preventDefault();
                files.view.dropBox.classList.remove('dragged');
                return false;
            }
            this.dropBox.ondrop = function(e) {
                e.preventDefault();
                files.view.dropBox.classList.remove('dragged');
                files.read(e.dataTransfer.files);
                return false;
            }
        }
    }
}

// Here we work with web-camera:
// create containers, move stream to video-element, make photos from camera
var camera = {

    run: function() {
        var videoObj = { "video": true };
        var userMedia = function(opt, sc, ec) { navigator.getUserMedia(opt, sc, ec)};
        if(navigator.webkitGetUserMedia)
            userMedia = function(opt, sc, ec) { navigator.webkitGetUserMedia(opt, sc, ec)};
        if(navigator.mozGetUserMedia)
            userMedia =  function(opt, sc, ec) { navigator.mozGetUserMedia(opt, sc, ec)};

        document.getElementById('camera').innerHTML = 'Для использования камеры необходимо дать разрешение браузеру';
        userMedia(videoObj, function(stream) {
            camera.stream.start(stream);
            camera.stream.data = stream;
        }, camera.stream.error);
    },

    stop: function() {
        if(camera.stream.data) {
            camera.stream.data.stop();
        }
        //camera.view.video.pause();
        camera.view.video.src = '';
    },

    stream: {
        data: '',
        start: function(stream) {
            camera.view.draw();
            camera.view.video.src = window.webkitURL.createObjectURL(stream);
            camera.view.video.play();
        },
        error: function(e) {
            console.log('Stream error', e);
            //$('#camera-modal').modal('hide');
            //$('#new-mail').show();
            //camera.stop();
        }
    },

    view: {
        width: 280,
        height: 210,
        video: '',

        draw: function() {
            document.getElementById('camera').innerHTML = '' +
                '<video id="video" width="'+camera.view.width+'" height="'+camera.view.height+'" autoplay></video>' +
                '<canvas id="canvas" width="'+camera.view.width+'" height="'+camera.view.height+'"></canvas>' +
                '<a id="snap" href="javascript:void(0)" class="btn "><i class="icon-camera"></i> Shoot!</a>'
                ;

            camera.view.video = document.getElementById("video");
            camera.view.photo.container = document.getElementById("canvas");
            camera.view.photo.context = camera.view.photo.container.getContext("2d");

            camera.view.photo.bind();
        },

        photo: {
            container: '',
            context: '',
            bind: function() {
                document.getElementById("snap").addEventListener("click", function() {
                    camera.view.photo.context.drawImage(camera.view.video, 0, 0, camera.view.width, camera.view.height);
                });
            },
            save: function() {
                var dataUrl = camera.view.photo.container.toDataURL("image/png");
                if(dataUrl) {
                    attachment.add(dataUrl);
                }
            }
        }
    }
}
