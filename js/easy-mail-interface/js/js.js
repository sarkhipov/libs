
function resizeColums() {
    var fh = $('.fixed-height').height()+60;
    var dh = $(window).height();
    $('.full-height').height(dh - fh);
}

$(document).ready(function(){

    // scrollbars
    var scrollOpt = {cursorcolor:"#48728f", cursorwidth: 4, cursoropacitymax: 0.3 };
    $('.message-list-container').niceScroll(scrollOpt);
    $('.mail-content').niceScroll({cursorcolor:"#48728f", cursorwidth: 4, cursoropacitymax: 0.3 });

    // init tooltips
    $('.active-tooltip').tooltip();

    // typeheads autocomplete
    $('.typeahead').typeahead({source: ['Ситибанк', 'Серега Т', 'Коля Б']})

    // menu clicks
    $('.message-list li').click(function(){
        $('.message-list li').removeClass('active');
        $(this).removeClass('new');
        $(this).addClass('active');
    });

    $('.nav-left li').click(function(){
        $('.nav-left li').removeClass('active');
        $(this).addClass('active');
    });

    // columns resize
    resizeColums();
    $(window).resize(function(){
        resizeColums();
    })

    // modal send mail
    $('#sendmail').click(function() {

        $('#new-mail .control-group').removeClass('error');

        var mail = $('#EmailTo').val();
        var subject = $('#EmailSubject').val();
        var send = true;

        if(!mail) {
            $('.group-email').addClass('error');
            setTimeout(function(){
                $('.group-email').removeClass('error');
            }, 1500);
            send = false;
        }
        if(!subject) {
            $('.group-subject').addClass('error');
            setTimeout(function(){
                $('.group-subject').removeClass('error');
            }, 1500);
            send = false;
        }

        if(send) {
            $('#send-success').fadeIn(300);
            $("#new-mail input").val("");
            $("#new-mail textarea").val("");
            $("#attachments").html('');
            setTimeout(function() {
                $('#send-success').hide();
                $('#new-mail').modal('hide');
            }, 1500)
        }
    });


    files.run();

    // camera modal bind
    $('#show-camera').click(function() {
        $('#camera-modal').modal('show');
        $('#new-mail').hide();
        camera.run();
    });
    $("#camera-cancel").click(function(){
        $('#camera-modal').modal('hide');
        $('#new-mail').show();
        camera.stop();
    });
    $("#camera-add").click(function(){
        $('#camera-modal').modal('hide');
        $('#new-mail').show();
        camera.stop();

        camera.view.photo.save();
    });
})
