"""
Puls - small application than catch socket messages with application metrics
It aggregate and save metrics
Can use several data providers for different databases (PG as default)
"""
from libs.SaverStream import SaverStream

# run socket daemon
saver = SaverStream()
saver.init().run()