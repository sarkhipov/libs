"""
Puls backend example for Postgres
"""

import psycopg2
import psycopg2.extras
import json


class pgBackend(object):
    config = None
    _connection = None
    cursor = None

    def __init__(self, config):
        self.config = config

    def __str__(self):
        return "pgModel object"

    def connect(self):
        self._connection = psycopg2.connect(self.config['cdn'])
        self._connection.set_client_encoding('utf8')
        self._connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        self.cursor = self._connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

    def query(self, sql, params=None, withResult=True, fetchOne=True):
        result = None

        if withResult:
            self.cursor.execute(sql, params)
            if fetchOne:
                result = self.cursor.fetchone()
            else:
                result = self.cursor.fetchall()
            #self._connection.commit()
        else:
            self.cursor.execute(sql, params)
            result = self._connection.commit()
        return result

    def saveMetric(self, params):
        sql = "INSERT INTO puls_metrics (project, metric, alias, value, time) "
        sql += " VALUES ( %(project)s, %(metric)s, %(alias)s, %(value)s, %(time)s ); "
        self.connect()
        return self.query(sql, params, False)

    def getAvgMetric(self, project, metric, type='max',  alias=None, dFrom=None, dTo=None):
        sql = "SELECT "+type+"(value) FROM  puls_metrics"
        sql += " WHERE project = %(project)s"
        sql += ' AND metric = %(metric)s '
        params = {'project': project, 'metric': metric}
        if(alias):
            sql += 'AND metric = %(metric)s '
            params['metric'] = metric
        if(dFrom):
            sql += 'AND date_created > %(dateFrom)s '
            params['dateFrom'] = dFrom
        if(dTo):
            sql += 'AND date_created = %(dateTo)s '
            params['dateTo'] = dTo
        sql += ' GROUP BY metric '
        self.connect()
        return self.query(sql, params, True)

    def getAvgMetricsByPeriod(self, project, metric, type='max', period='minute', alias=None, dFrom=None, dTo=None):
        sql = "SELECT "+type+"(value), date_trunc('"+period+"',date_created)::timestamp as date "
        sql += "FROM puls_metrics "
        sql += "WHERE project=%(project)s "
        sql += "AND metric = %(metric)s "
        params = {'project': project, 'metric': metric}
        if(alias):
            sql += 'AND metric = %(metric)s '
            params['metric'] = metric
        if(dFrom):
            sql += 'AND date_created > %(dateFrom)s '
            params['dateFrom'] = dFrom
        if(dTo):
            sql += 'AND date_created = %(dateTo)s '
            params['dateTo'] = dTo
        sql += "GROUP BY date_trunc('"+period+"', date_created) "
        sql += "ORDER BY date desc limit 30"
        self.connect()
        return self.query(sql, params, True, False)

