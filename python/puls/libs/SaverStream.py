"""
Thanks Vladimir Yesaulov for this class :)

Main lib class
Prepare daemon for catch socket messages
Collect message queue and put it into aggregator (or by timer interval)
Run aggregation process in separate thread
"""

import tornado

import time
import tornado.ioloop
import socket
import functools
import errno
import threading

from configobj import ConfigObj
import os
import json

from libs.backends.pgBackend import pgBackend
from libs.Aggregator import Aggregator
from libs.Api import Api


class SaverStream(object):
    host = ''
    port = 43278
    connections = 128

    socket = None
    backend = None
    config = None
    messages = {}
    aggregator = None
    api = None


    def __init__(self):
        self.loadConfig()
        self.getDbConnection()
        self.aggregator = Aggregator(self.backend)
        self.api = Api(self.backend)

    def init(self, host=None, port=None, connections=None):
        if host != None:
            self.host = host
        if port != None:
            self.port = port
        if connections != None:
            self.connections = connections

        self.createSocket()
        #self.getDbConnection()

        return self

    def loadConfig(self):
        if self.config == None:
            configFile = os.path.dirname(__file__) + "/../config/config.ini"
            self.config = ConfigObj(configFile)
            #print self.config['global']
            self.host = self.config['global']['host']
            self.port = int(self.config['global']['port'])

    def createSocket(self):
        if self.socket == None:
            print "create socket ..."
            print self.socket

            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.socket.setblocking(0)
            self.socket.bind((self.host, self.port))
            self.socket.listen(self.connections)

            #print self.socket

    def timer(self):
        #print "run timer ... "
        self.runInThread()
        run = getattr(self, 'timer')
        threading.Timer(float(self.config['global']['inMemoryCacheLifeTime']), run).start()

    def run(self):
        self.timer()
        self.io_loop = tornado.ioloop.IOLoop.instance()
        callback = functools.partial(self.connection_ready, self.socket)
        self.io_loop.add_handler(self.socket.fileno(), callback, self.io_loop.READ)
        self.io_loop.start()

    def close(self):
        self.ioloop.remove_handler(self.socket.fileno())
        self.socket.close()
        self.socket = None

    def connection_ready(self, sock, fd, events):
        while True:
            try:
                connection, address = sock.accept()
            except socket.error, e:
                if e.args[0] not in (errno.EWOULDBLOCK, errno.EAGAIN):
                    # @todo log this
                    raise
                return

            #connection.setblocking(0)
            message = connection.recv(4096)
            #print "M"

            if message != "":
                self.messages[time.time()] = message

            # @todo check for last message time
            if len(self.messages) >= int(self.config['global']['inMemoryCacheSize']):
                self.runInThread(self.messages)
                self.messages = {}

    def runInThread(self, items=None):
        if items == None:
            items = self.messages

        if len(items) == 0:
            return

        run = getattr(self, 'acceptData')
        t = threading.Thread(target=run, args=( {json.dumps({'items': items})} ))
        t.setDaemon(0)
        t.start()

    #Ipee.app().threads[time.time()] = t
    #Ipee.app().setCounter()

    def acceptData(self, data={}):
        print time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

        try:
            data = json.loads(data)
            items = data['items']
            print "items count = " + str(len(items))
            self.aggregator.run(items)
        except Exception as inst:
            print "BAD DATA"
            print type(inst)
            print inst.args
            return False


    def getDbConnection(self):
        if self.backend != None:
            return

        if self.config['db']['backend'] == 'pg':
            self.backend = pgBackend(self.config['db'])

        return True


    @staticmethod
    def app(x=''):
        if x == '':
            x = SaverStream
        try:
            single = x()
        except SaverStream, s:
            single = s
        return single
