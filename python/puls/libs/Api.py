"""
Puls API class
Some methods to collect metrics from DB
"""

import json
import datetime

class Api:

    backend = None

    def __init__(self, backend):
        self.backend = backend

    def getAvgMetric(self, **kwargs):
        data = self.backend.getAvgMetric(
            kwargs['project'],
            kwargs['metric'],
            kwargs['type'],
            kwargs['alias'],
            kwargs['dateFrom'],
            kwargs['dateTo']
        )
        if data[0]:
            data = data[0]
        return self.success(data)

    def getAvgMetricByPeriod(self, **kwargs):
        data = self.backend.getAvgMetricsByPeriod(
            kwargs['project'],
            kwargs['metric'],
            kwargs['type'],
            kwargs['period'],
            kwargs['alias'],
            kwargs['dateFrom'],
            kwargs['dateTo']
        )
        result = []
        formats = {'m': "{:10.0f}", 'pm': "{:10.0f}", 'mt':"{:10.4f}"}
        for item in data:
            result.append({
                'value': formats[kwargs['metric']].format(item[0]).strip(),
                'date': item[1].strftime("%H:%M %d.%m.%Y")
            })

        #return reversed list
        return self.success(result[::-1])

    # response methods
    def response(self, data):
        return json.dumps(data)

    def success(self, data):
        return self.response({"error": 0, "data": data})

    def error(self, code, desc):
        return self.response({"error": 1, "code": code, "desc": desc, "data": []})