"""
Puls Aggregator class
Collect metrics by type and period, sum and aggregate and save in DB
"""

import json
import datetime

class Aggregator:

    backend = None

    def __init__(self, backend):
        self.backend = backend

    def run(self, data):
        for key in data:
            self.handleItem(data[key])
        return True

    def handleItem(self, data):
        data = json.loads(data)
        systemMetrics = ["a", "p", "t"]

        for metric in data:
            if not metric in systemMetrics:
                self.backend.saveMetric({
                    'project': data['p'],
                    'alias': data['a'],
                    'time': datetime.datetime.fromtimestamp(int(data['t'])).strftime('%Y-%m-%d %H:%M:%S'),
                    'metric': metric,
                    'value': data[metric]
                })
        return True