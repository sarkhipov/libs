"""
Puls API
Tornado based application for handle http requests
Provide to get application metrics from Puls
Use class Api for work with metrics in db
"""

import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
from tornado.options import define, options

from libs.SaverStream import SaverStream

define("port", default=8888, help="run on the given port", type=int)

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r'/getMetrics', GetMetrics),
        ]
        settings = dict(
            cookie_secret="43oETzKXsdavsvkwefL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
            xsrf_cookies=True,
            autoescape=None,
        )
        tornado.web.Application.__init__(self, handlers, **settings)

# Default handler
class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Welcome to Apps3 Puls Api")

# Get metric values from DB
class GetMetrics(tornado.web.RequestHandler):
    def get(self):
        result = ''
        saver = SaverStream()
        period = self.get_argument("period", default=None)
        type = self.get_argument("type", default='count')
        project = self.get_argument("project", default=None)
        metric = self.get_argument("metric", default=None)
        alias = self.get_argument("alias", default=None)
        dFrom = self.get_argument("dateFrom", default=None)
        dTo = self.get_argument("dateTo", default=None)

        if type == "max" or type == "avg" or type == "count":
            # if we have time period, get values list divided by period
            if period:
                result = saver.api.getAvgMetricByPeriod(project=project, metric=metric, type=type, period=period,
                                                        alias=alias, dateFrom=dFrom, dateTo=dTo)
            # else get one metric value
            else:
                result = saver.api.getAvgMetric(project=project, metric=metric, type=type,
                                                alias=alias, dateFrom=dFrom, dateTo=dTo)

        self.write(result)

# Run Tornado web application
def main():
    tornado.options.parse_command_line()
    app = Application()
    app.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()